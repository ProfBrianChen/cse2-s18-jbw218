//This program asks the user for doubles
//that represent the number of acres of land affected
//by hurricane precipitation and how many inches of rain were dropped on average
// and converts the quantity of rain into cubic miles.

import java.util.Scanner;

public class Convert {
    //main method required for every Java program
  public static void main(String[] args) {
    
    double area = 0; //defines area
    Scanner scan = new Scanner(System.in); //prompt user to input area in acres
    System.out.println("Enter the affected area in acres");
    area = scan.nextDouble();
    double rain = 0; //defines amount of rainfall
    System.out.println("Enter the rainfall in the affected area in inches"); //prompt user to input affected area
    rain = scan.nextDouble();
    
    double cubicMiles = 0; //defines cubic miles
    cubicMiles += area * 0.0015625; //converts acres to cubicMiles
    cubicMiles += rain * .00000000000000393; //converts inches to cubic miles
    
    System.out.println("Area affected in acres: " + area); //prints area affected in acres
    System.out.println("Amount affected in inches: " + rain); //prints amount affected in inches
    System.out.println(cubicMiles + " cubic miles"); //prints area affected in cubic miles
  
  }

}
