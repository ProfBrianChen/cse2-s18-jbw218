//Jack Wallace
//2-13-18
//Prof. Chen
//Section 211

//This program prompts the user for the dimensions of a square pyramid and returns the volume inside the pyramid.

import java.util.Scanner;

public class Pyramid {
  //main class for all java programs
  public static void main(String[] args) {
    
    Scanner scan = new Scanner(System.in);
    
    double square = 0; //defines square dimension variable
    System.out.println("Please enter the square side of the pyramid:"); //prompts user for one side of the pyramid
    square = scan.nextDouble();
    
    double height = 0; //defines height variable
    System.out.println("Please enter the height of the pyramid:"); //prompts user for the height of the pyramid
    height = scan.nextDouble();
    
    double volume = 0; //defines volume variable
    volume += square * square; //area of base
    volume *= height; //multiplied by the height
    volume /= 3; //divided by 3
    
    System.out.println("Square side: " + square); //prints the square side
    System.out.println("Height: " + height); //prints the height 
    System.out.println("Volume: " + volume); //prints the volume
    
  }
  
  
  //end of class
}