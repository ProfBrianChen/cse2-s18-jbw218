//Jack Wallace
//CSE2
//Prof. Chen
//hw09
import java.util.Scanner;
import java.util.Random;
public class DrawPoker{
  public static void main(String[]args){
    Scanner scan=new Scanner(System.in);
    int choice=0;
    System.out.println("Press any key to start the game...");
    scan.next();
    int deck[]=initGame();
    do{
      for(int i =0; i<=51; i++) {
        //System.out.print(deck[i]+" ");  
      }
      System.out.println();
      System.out.println("Shuffling deck...");
      int shufDeck[]=shuffle(deck);
      for(int i =0; i<=51; i++) {
        //System.out.print(shufDeck[i]+" "); //check if shuffled
      }
      System.out.println();
      
      int[] p1=new int[5];
      int[] p2=new int[5];

      int q = 0;
      int r = 0;

      for(int i =0; i<10;i++) {
        if (i%2==0){
          p1[q]=shufDeck[i];
          q++;
        }
        else {
          p2[r]=shufDeck[i];
          r++;
        }
      }

      String hand1=printCards(p1);
      System.out.println("Player 1 Hand: "+hand1);
      String hand2=printCards(p2);
      System.out.println("Player 2 Hand: "+hand2);
      
      System.out.println("Player 1:");
      if(pair(p1)) {
        System.out.println("Pair");
      }
      else if(threekind(p1)) {
        System.out.println("Three Kind");
      }
      else if(flush(p1)) {
        System.out.println("Flush");
      }
      else if(fullhouse(p1)) {
        System.out.println("Full House");
      }
      else {
        System.out.println("High Card");
      }
      
      System.out.println("Player 2:");
      if(pair(p2)) {
        System.out.println("Pair");
      }
      else if(threekind(p2)) {
        System.out.println("Three Kind");
      }
      else if(flush(p2)) {
        System.out.println("Flush");
      }
      else if(fullhouse(p2)) {
        System.out.println("Full House");
      }
      else {
        System.out.println("High Card");
      }
      
      
      System.out.println("Enter 1 to play again, or 0 to quit...");
      choice=scan.nextInt();
    } while(choice==1);
    
  }
  
  public static boolean pair(int[] hand) {
    for (int i =0;i<5;i++) {
      hand[i]=hand[i]%13; //break hand into ranks
    }
    for(int i =0; i<5;i++){
      for(int j =0; j<5; j++) {
        if(i==j){ //if same index skip
          continue;
        }
        if(hand[i]==hand[j]) { //if two match
          if(threekind(hand)==true) { //check not three of a kind
            return false;
          }
          return true; //otherwise pair
        }
      }
    }
    return false;
} //check if pair

  public static boolean threekind(int[] hand) {
    for (int i =0;i<5;i++) {
      hand[i]=hand[i]%13; //break hand down to ranks
    }
    for(int i =0; i<5;i++){
      for(int j =0; j<5; j++) {
        for(int k=0; k<5; k++){
        if(i==j || j==k || i==k){ //check if 3 cards are equal
        continue;  
        }
        if(hand[i]==hand[j] && hand[i]==hand[k]) { //if true return three of a kind
          return true;
        }
       }
      }
     }
    return false;
  } //check if three of a kind
  
  public static boolean flush(int[] hand) { //check if flush
    int count1 = 0;
    int count2 = 0;
    int count3 = 0;
    int count4 = 0;
    
    for (int i =0; i<5; i++) {
      if (hand[i]<=12){
        count1++;
      }
      else if(hand[i]>12 && hand[i]<=25){
        count2++;
      }
      else if(hand[i]>25 && hand[i]<=38){
        count3++;
      }
      else if(hand[i]>38 && hand[i]<=51){
        count4++;
      }
      else{
        return false;
      } 
    }
    if(count1==5||count2==5||count3==5||count4==5) { //if counters equal 5 then all cards are same suit
      return true;
    }
    return false;
  } //check if flush
  
  public static boolean fullhouse(int[] hand) { 
    boolean threecount=false;
    boolean paircount=false;
    
    for (int i=0;i<5;i++) {
      hand[i]=hand[i]%13;
    }
    
    int rank[] = new int[13]; //array of ranks
    for (int i =0; i< 13; i++) {
      rank[i]=0; //fill all with 0
    }
    for (int i =0; i< 5; i++) {
      int number = hand[i]; //set number to rank
      
      rank[number] = rank[number]+1;//array counter
    }
    for(int i =0; i<13;i++) { //for each rank
      if(rank[i]==2) { //if theres 2, at least 2 pair
        paircount=true;
        break;
      }
    }
    for(int i =0; i<13;i++) { 
      if(rank[i]==3) { //if theres 3, at least 3 of a kind
        threecount=true;
        break;
      }
    }
    
    if(threecount==true && paircount==true) { //if both, then full house
      return true;
    }
   
    return false;
  } //check if fullhouse
  
  public static int[] initGame(){
    int deck[]=new int[52];
    for (int i=0; i<=51; i++){
      deck[i]=i; //initiate deck
    }
    return deck;
  } //initilize game
  
  public static int[] shuffle(int deck[]){
    int temp=0;
    for(int i =0; i<=51; i++) {
      int j=(int)(Math.random()*52); //pick random position in deck
      temp=deck[j]; //take index at random position
      deck[j]=deck[i]; //then swap existing value with random
      deck[i]=temp;
    }
    return deck;
  } //shuffles deck
  
  public static String printCards(int[] cards){
    String suit="";
    String card="";
    
    String hand="";
  
    for (int i=0; i<cards.length; i++) {
      //get suit
      if(cards[i]<=12){
        suit="Diamonds";
      }
      else if(cards[i]>12&&cards[i]<=25){
        suit="Clubs";
      }
      else if(cards[i]>24&&cards[i]<=38){
        suit="Hearts";
      }
      else {
        suit="Spades";
      }
      
      //get number
      int t = cards[i]%13;
      if(t==0) {
        card="Ace";
      }
      else if(t==12){
        card="King";
      }
      else if (t==11){
        card="Queen";
      }
      else if(t==10){
        card="Jack";
      }
      else {
        card= t+"";
      }
      hand+=(card+"-"+suit+"  ");
      //reset
      card="";
      suit="";
    }
    return hand;
  }

}

