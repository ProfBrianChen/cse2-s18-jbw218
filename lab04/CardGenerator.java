//This program picks a random card from a standard deck of 52 cards

public class CardGenerator {
  // main method required for every Java program
  public static void main(String [] srgs) {
    
    
  int suit = (int) (Math.random() * 4 + 1); //creates random number from 1-4 for suits
    
    String suitString; //replacing numbers with suits
    switch (suit) {
      case 1: suitString = "Diamonds";
        break;
      case 2: suitString = "Clubs";
        break;
      case 3: suitString = "Hearts";
        break;
      case 4: suitString = "Spades";
        break;
        
    }
    
    int card = (int) (Math.random() * 13 + 1); //creates random number from 1-13 for cards
    
    String cardString; //replaces 1,11,12,13 with card names
    switch (card) {
      case 1: cardString = "Ace";
        break;
      case 11: cardString = "Jack";
        break;
      case 12: cardString = "Queen";
        break;
      case 13: cardString = "King";
        break;
           
    }
  System.out.println(cardString) ; 
    System.out.println(suitString) ;
    
  }
}