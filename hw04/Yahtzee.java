import java.util.Scanner;

public class Yahtzee {
  //main class for all java programs
  public static void main(String[] args) {
    System.out.println("Press 1 for random roll or 2 to input a roll");
    Scanner scan = new Scanner(System.in);
    int c = scan.nextInt();
    if (c<1 || c> 2){
      System.out.println("Please enter a valid choice"+c);
      return;
    }
    System.out.println();
    //declare die
    int d1=0;
    int d2=0;
    int d3=0;
    int d4=0;
    int d5=0;
    
    if (c==1) {
      d1 = (int) (Math.random() * 6 + 1); //creates random number from 1-6 for die
      d2 = (int) (Math.random() * 6 + 1); //creates random number from 1-6 for die
      d3 = (int) (Math.random() * 6 + 1); //creates random number from 1-6 for die
      d4 = (int) (Math.random() * 6 + 1); //creates random number from 1-6 for die
      d5 = (int) (Math.random() * 6 + 1); //creates random number from 1-6 for die
      System.out.println(d1); //print check for die numbers
      System.out.println(d2);
      System.out.println(d3);
      System.out.println(d4);
      System.out.println(d5);
    }
    else if (c==2){
      System.out.println("Please enter 5 integers from 1-6");
      int r=scan.nextInt();
      //error check that integer is within min and max values
      if (r>=11111 || r<=66666){
        d1=r % 10; //last digit remainder
        r=r/10; //cut off last digit
        d2=r % 10; //repeats 5 times to count for every digit
        r=r/10;
        d3=r % 10;
        r=r/10;
        d4=r % 10;
        r=r/10;
        d5=r % 10;
        r=r/10;
        System.out.println(d1); //print check for die numbers
        System.out.println(d2);
        System.out.println(d3);
        System.out.println(d4);
        System.out.println(d5);
        //error checking for each digit ensuring they are 1-6
        if (d1 <1 || d1 >6){
            System.out.println("Numbers have to be 1-6");
            return; 
        }
        if (d2 <1 || d2 >6){
            System.out.println("Numbers have to be 1-6");
            return; 
        }
        if (d3 <1 || d3 >6){
            System.out.println("Numbers have to be 1-6");
            return; 
        }
        if (d4 <1 || d4 >6){
            System.out.println("Numbers have to be 1-6");
            return; 
        }
        if (d5 <1 || d5 >6){
            System.out.println("Numbers have to be 1-6");
            return; 
        }
     }
     else{
       System.out.println("Please enter 5 integers within 11111 and 66666"); //ask for 5 dice numbers
     }
    }
    //get values of upper section
    int aces=0;
    int twos=0;
    int threes=0;
    int fours=0;
    int fives=0;
    int sixes=0;
    
    if (d1==1){ //score of die 1
      aces=aces+1;
    }
    else if (d1==2){
      twos=twos+1;
    }
    else if (d1==3){
      threes=threes+1;
    }
    else if (d1==4){
      fours=fours+1;
    }
    else if (d1==5){
      fives=fives+1;
    }
    else if (d1==6){
      sixes=sixes+1;
    }
    if (d2==1){ //score of die 2
      aces=aces+1;
    }
    else if (d2==2){
      twos=twos+1;
    }
    else if (d2==3){
      threes=threes+1;
    }
    else if (d2==4){
      fours=fours+1;
    }
    else if (d2==5){
      fives=fives+1;
    }
    else if (d2==6){
      sixes=sixes+1;
    }
    if (d3==1){ //score of die 3
      aces=aces+1;
    }
    else if (d3==2){
      twos=twos+1;
    }
    else if (d3==3){
      threes=threes+1;
    }
    else if (d3==4){
      fours=fours+1;
    }
    else if (d3==5){
      fives=fives+1;
    }
    else if (d3==6){
      sixes=sixes+1;
    }
    if (d4==1){ //score of die 4
      aces=aces+1;
    }
    else if (d4==2){
      twos=twos+1;
    }
    else if (d4==3){
      threes=threes+1;
    }
    else if (d4==4){
      fours=fours+1;
    }
    else if (d4==5){
      fives=fives+1;
    }
    else if (d4==6){
      sixes=sixes+1;
    }
    if (d5==1){ //score of die 5
      aces=aces+1;
    }
    else if (d5==2){
      twos=twos+1;
    }
    else if (d5==3){
      threes=threes+1;
    }
    else if (d5==4){
      fours=fours+1;
    }
    else if (d5==5){
      fives=fives+1;
    }
    else if (d5==6){
      sixes=sixes+1;
    }
    //print count of upper score
    System.out.println("Aces: "+aces);
    System.out.println("Twos: "+twos);
    System.out.println("Threes: "+threes);
    System.out.println("Fours: "+fours);
    System.out.println("Fives: "+fives);
    System.out.println("Sixes: "+sixes);
    int upperscore=(aces*1+twos*2+threes*3+fours*4+fives*5+sixes*6);
    System.out.println("Score of Upper Section: "+(upperscore)); //print value of upper score
    int upperscoreb=0;
    if (upperscore>63){
      upperscoreb=upperscore+35; //upperscore with bonus
      
    }
    System.out.println("Score of Upper Section with bonus: "+(upperscoreb)); //print value of upper score with bonus
    
    //declare lower section counts
    int threeofakind=0;
    int fourofakind=0;
    int fullhouse=0;
    int sstraight=0;
    int lstraight=0;
    int yahtzee=0;
    //get lower section counts
   if (aces==3 || twos==3 || threes==3 || fours==3 || fives==3 || sixes==3){ // 3 of a kind and maybe fullhouse
     threeofakind=1;
     System.out.println("Three of a kind!");
     if (aces==2 || twos==2 || threes==2 || fours==2 || fives==2 || sixes==2){  //check if fullhouse
         fullhouse=1;
       System.out.println("Fullhouse!");
       }
   }
   if (aces==4 || twos==4 || threes==4 || fours==4 || fives==4 || sixes==4){ //check if four of a kind
         fourofakind=1;
     System.out.println("Four of a kind!");
   }
   if (aces==5 || twos==5 || threes==5 || fours==5 || fives==5 || sixes==5){ //check if yahtzee
       yahtzee=1;
     System.out.println("Yahtzee!");
   }
   if (twos==1 && threes==1 && fours==1 && fives==1 ){ //if each inside die appears once then large straight
     lstraight=1;
     System.out.println("Large Straight!");
   }
   if (threes>=1 && fours>=1 && ( (twos>=1 && fives>=1) || (twos>=1 && aces>=1) || (fives>=1 && sixes>=1) )){ //3 and 4 must be for small straight and two ajacent to have small straight
     sstraight=1;
     System.out.println("Small Straight!");
   }
     //print lower score counts
     //System.out.println("")
   //disregard chance because it equals upperscore
   int lowerscore=(yahtzee*50+fullhouse*25+sstraight*30+lstraight*40+upperscore*threeofakind+upperscore*fourofakind+upperscore); //calculate lower score
   System.out.println("Score of Lower Section: "+(lowerscore)); //total of lower section
   System.out.println("Grand total: "+(lowerscore+upperscoreb)); //upper section plus lower section for grand total
   
  }
}
