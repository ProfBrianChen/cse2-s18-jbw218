//Program to split a bill evenly among a group of friends.
//This program demonstates the use of the Scanner class

import java.util.Scanner;

public class Check{
  //main method required for every Java program
  public static void main (String[] args) {
    Scanner myScanner = new Scanner( System.in );
    //prompt user for the original cost of the check
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //Now accept user input
    double checkCost = myScanner.nextDouble(); //tells Java that you need a
    //method that is a part of the myScanner object
    //Prompt user for the tip percentage they want
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    //calculate tip percentage
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    //Prompt user to input how many people went out to dinner
      System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); //creates number of people
    
    //Print out output
    double totalCost; //create the total cost variable
    double costPerPerson; //create the cost per person variable
    int dollars,  //whole dollar amount of cost
        dimes, pennies;  //for storing digits to the right
                         // decimal point for the cost$
    totalCost = checkCost * (1+ tipPercent);
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    //get dimes amount
    dimes=(int)(costPerPerson * 10) % 10;
    //get pennies amount
      pennies=(int)(costPerPerson * 100) % 10;
    //print out total amount each person owes
      System.out.println("Each person in the group owes $" +
                        dollars + '.' + dimes + pennies);
  } //end of main method
} //end of class
