//Jack Wallace
//CSE2
//hw07
//Prof. Chen
//for getting used to methods
import java.util.Scanner;
public class StringAnalysis{
  public static void main (String [] Args){
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter a String ");
    String s=scan.next();
    System.out.println("Enter 0 for examining all characters, 1 for a specific amount ");
    int choice=scan.nextInt();
    while (choice!=1 && choice!=0){
      System.out.println("Invalid choice ");
      choice=scan.nextInt();
    }
    if (choice==0){
      boolean a=checkalpha(s);
      if (a==true){
        System.out.println("All characters in String are letters ");
      }
      else{
        System.out.println("All characters are not letters ");
      }
    }
    else if (choice==1){
      System.out.println("How many characters do you wish to inspect? ");
      int charnumber=scan.nextInt();
      boolean a=checkalpha(s,charnumber);
      if (a==true){
        System.out.println("All characters in String are letters ");
      }
      else{
        System.out.println("All characters are not letters ");
      }
    }  
  }
  public static boolean checkalpha(String s){
    //System.out.println(s.length());
    for (int i=0; i<s.length(); i++){
      char c=s.charAt(i);
      //System.out.println(c);
      if ((c>='a' && c<='z') || (c>='A' && c<='Z')){  //if lower or upper bounds within characters
        continue;
      }
      else{
        return false;
      }
    }
    return true;
  }
  public static boolean checkalpha(String s, int charnumber){
    System.out.println(charnumber);
    if (charnumber>s.length()){  //if its greater than size, then run first method
        System.out.println("Characters exceed the length of the string. ");
        System.out.println("Checking all characters... ");
        checkalpha(s);
      }
    for (int i=0; i<charnumber; i++){
      char c=s.charAt(i);
      //System.out.println(c);
      if ((c>='a' && c<='z') || (c>='A' && c<='Z')){  //if lower or upper bounds within characters
        continue;
      }
      else{
        return false;
      }
    }
    return true;
  }
}