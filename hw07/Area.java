//Jack Wallace
//CSE2
//hw07
//Prof. Chen
// this program asks for a shape, dimensions, and determines area
//for getting used to methods
import java.util.Scanner;
public class Area {
  public static double rectArea( double width, double height){
    return width*height;
  }
  public static double triArea( double width, double height){
    return .5*width*height;
  }
  public static double circArea( double radius){
    double PI=3.14159;
      return PI*radius*radius;
  }
  public static void main (String [] Args){
    Scanner scan = new Scanner(System.in);
    String shape="";
    String rectangle="";
    String triangle="";
    String circle="";
    System.out.println("Please enter a valid shape: ");
    shape=scan.next();
    while (!shape.equals("rectangle") && !shape.equals("triangle") && !shape.equals("circle")){
    System.out.println("Invalid shape, please choose either a rectangle triangle or circle: ");
    shape=scan.next();
    }
    if (shape.equals("rectangle")){
      double width;
      double height;
      System.out.println("Please enter the width: ");
      width=scan.nextDouble();
      System.out.println("Please enter the height: ");
      height=scan.nextDouble(); 
      double area=rectArea(width,height);
      System.out.println("Area: "+area);
    }
    else if (shape.equals("triangle")){
      double width;
      double height;
      System.out.println("Please enter the width: ");
      width=scan.nextDouble();
      System.out.println("Please enter the height: ");
      height=scan.nextDouble(); 
      double area=triArea(width,height);
      System.out.println("Area: "+area);
    }
    else if (shape.equals("circle")){
      double radius;
      System.out.println("Please enter the radius: ");
      radius=scan.nextDouble();
      double area=circArea(radius);
      System.out.println("Area: "+area);
    }
    else {
      System.out.println("Something went wrong: "+shape);
    }
  }
}