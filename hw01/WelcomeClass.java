//Displays a welcome from me using my Leigh network ID
//as a signature

public class WelcomeClass{
    // main method required for every Java program
  public static void main(String [] args){
    //Print statements
    System.out.println("-------------");
    System.out.println(" | WELCOME |");
    System.out.println("-------------");
    System.out.println("  ^  ^  ^  ^  ^  ^ \n / \\/ \\/ \\/ \\/ \\/ \\"); //formatting including a new line
    System.out.println("<-J--B--W--2--1--8->");  //My lehigh login ID
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /  \n  v  v  v  v  v  v"); //formatting including a new line
    System.out.println("My name is Jack Wallace. I am a senior");  //about me
    System.out.println("Mechanical engineer student who wanted to learn");
    System.out.println("Java. I am looking forward to learning more");
    System.out.println("about this class and java.");
  } //end of main method
} //end of class