import java.util.Scanner;
import java.util.Random;
public class TexasHoldem{
  public static void main(String[]args){
    Scanner scan=new Scanner(System.in);
    int choice=0;
    System.out.println("Press any key to start the game...");
    scan.next();
    do{
      int deck[]=initGame();
      for(int i =0; i<=51; i++) {
        //System.out.print(deck[i]+" ");
      }
      System.out.println();
      System.out.println("Shuffling deck...");
      int shufDeck[]=shuffle(deck);
      for(int i =0; i<=51; i++) {
        //System.out.print(shufDeck[i]+" ");
      }
      System.out.println();
      
      dealHands(shufDeck);
      
      System.out.println("Press any key for...");
      scan.next();
 
      System.out.println("THE FLOP: ");
      int flop[] = new int[3];
      for(int i =0; i<3; i++){
        flop[i]=deck[i+4];
      }
      System.out.println(printCards(flop));
      
      System.out.println("Press any key for...");
      scan.next();
      System.out.println("THE TURN: ");
      int turn[] = new int[4];
      for(int i =0; i<4; i++){
        turn[i]=deck[i+4];
      }
      System.out.println(printCards(turn));
      
      System.out.println("Press any key for...");
      scan.next();
      System.out.println("THE RIVER!");
      
      int river[] = new int[5];
      for(int i =0; i<5; i++){
        river[i]=deck[i+4];
      }
      System.out.println(printCards(river));
      
      System.out.println("Enter 1 to play again, or 0 to quit...");
      choice=scan.nextInt();
    } while(choice==1);
    
  }
  public static int[] initGame(){
    int deck[]=new int[52];
    for (int i=0; i<=51; i++){
      deck[i]=i; //initiate deck
    }
    return deck;
  }
  
  public static int[] shuffle(int deck[]){
    int temp=0;
    for(int i =0; i<=51; i++) {
      int j=(int)(Math.random()*52);
      temp=deck[j];
      deck[j]=deck[i];
      deck[i]=temp;
    }
    return deck;
  }
  
  public static void dealHands(int deck[]){
    int[] p1=new int[2];
    int[] p2=new int[2];
    
    p1[0]=deck[0];
    p2[0]=deck[1];
    p1[1]=deck[2];
    p2[1]=deck[3];
   
    String hand1=printCards(p1);
    System.out.println("Player 1 Hand: "+hand1);
    String hand2=printCards(p2);
    System.out.println("Player 2 Hand: "+hand2);
  }
  
  public static String printCards(int[] cards){
    String suit="";
    String card="";
    
    String hand="";
  
    for (int i=0; i<cards.length; i++) {
      //get suit
      if(cards[i]<=12){
        suit="D";
      }
      else if(cards[i]>12&&cards[i]<=25){
        suit="C";
      }
      else if(cards[i]>24&&cards[i]<=38){
        suit="H";
      }
      else {
        suit="S";
      }
      
      //get number
      int t = cards[i]%13;
      if(t==0) {
        card="A";
      }
      else if(t==12){
        card="K";
      }
      else if (t==11){
        card="Q";
      }
      else if(t==10){
        card="J";
      }
      else {
        card= t+"";
      }
      hand+=(card+"-"+suit+"  ");
      //reset
      card="";
      suit="";
    }
    return hand;
  }
}



