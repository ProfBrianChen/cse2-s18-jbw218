import java.util.Scanner;
public class practice{
  public static void main(String[]args){
    Scanner scan =new Scanner(System.in);
    System.out.println("Enter a positive 5-digit integer");
    int n=0;
    boolean goodData;
    goodData=scan.hasNextInt();
    if (goodData){
    n=scan.nextInt();
    }
    if (!goodData || n<10000 || n>99999){
      System.out.println("Bad input");
      return;
    }
    if(n/10000==n%10 && n/10%10 ==n/1000%10){
      System.out.println("sysmmetric");
    }
    else{
      System.out.println("Assymmetric");
    }
  }  
}
