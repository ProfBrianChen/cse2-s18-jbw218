// Bicycle cyclometer to record data
// This program will: print the number of minutes for each trip
// print the number of counts for each trip
// print the distance of each trip in miles
// print the distance for the two trips combined

public class Cyclometer {
  // main method required for every Java program
  public static void main(String [] srgs) {
    int secsTrip1=480; //seconds for trip 1
    int secsTrip2=3220; //seconds for trip 2
    int countsTrip1=1561; //counts for trip 1
    int countsTrip2=9037; //counts for trip2
    
    double wheelDiameter=27.0, // wheel diameter
    PI=3.14159, // value of pi
    feetPerMile=5280, // number of feet in a mile 
    inchesPerFoot=12, // inches in a foot
    secondsPerMinute=60, //seconds in a minute
    double distanceTrip1, distanceTrips2,totalDistance;
    
    System.out.println("Trip 1 took "+
                      (secsTrip1/secondsPerMinute)+ " minutes and had "+
                      countsTrip1+" counts.");
    System.outprintln("Trip 2 took "+
                     (secsTrip2/secondsPerMinute)+" miuntes and had "+
                     countsTrip2+" counts."); //print statements
    //run the calculations; store the values
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    //Above gives distance in inches
    //(for each count, a rotation of the whhel travels
    //the diameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; //Gvies distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
  } // end of main method
} // end of class
