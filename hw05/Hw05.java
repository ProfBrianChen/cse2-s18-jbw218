//Jack Wallace
//CSE2
//hw05
//Prof. Chen
// this program asks for a students course specifics using loops
import java.util.Scanner;
public class Hw05 {
    //main method required for every Java program
  public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
    for (int number=0; number<100 || number >999;){ //using a for loop to enter a valid course number of 3 digits
      System.out.println("Enter a valid course number (XXX)");
      number = scan.nextInt();
    }
    String department=null;
    System.out.println("Enter a valid department (XX, XXX, or XXXX)");
    department = scan.next();
    while (department.length()<2 || department.length() >4){ //using a while loop to enter a valid course with 2,3, or 4 letters
    System.out.println("Error, enter a valid department (XX, XXX, or XXXX)");
    department = scan.next();
    }
    int week=0;
    while (week<1 || week>7){
      System.out.println("Enter a valid number of times a week you meet "); //using a while loop to enter number of times a week they meet from 1-7
      week=scan.nextInt();
    }
    int hour=0;
    while (hour>12 || hour<1){
      System.out.println("Enter the hour the class starts "); //using a while loop to enter the hour of the class start time from 1-12
      hour=scan.nextInt();
    }
    int minutes=-1;
    while (minutes>59 || minutes<0){ //using a while loop to determine the minutes the class starts from 0-59
      System.out.println("Enter the minute the class starts ");
      minutes=scan.nextInt();
    }
    String prof=null;
    System.out.println("What is the indtructers name? ");
    prof=scan.next();  
    int students=0;
    while (students<=0){ //using a while loop to enter number of students in the class 
      System.out.println("How many students are in the class?");
      students=scan.nextInt();
    }
    
    }
  }
