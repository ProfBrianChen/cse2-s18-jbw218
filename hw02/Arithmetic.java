// calculating total cost of each item listed


public class Arithmetic {
  // main method required for every Java program
  public static void main(String [] args) {

    int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

    double totalCostofPants; //total cost of pants
    double totalCostofSweatShirts; //total cost of sweatshirts
    double totalCostofBelts; //total cost of belts
    
    System.out.println("The total cost of pants is ");
    System.out.printf("%.2f \n",numPants*pantsPrice+numPants*pantsPrice*paSalesTax);
    //prints total cost of pants
    System.out.println("The total cost of sweatshirts is ");
    System.out.printf("%.2f \n",numShirts*shirtPrice+numShirts*shirtPrice*paSalesTax);
    //prints total cost of shirts
    System.out.println("The total cost of belts is ");
    System.out.printf("%.2f \n",numBelts*beltCost+numBelts*beltCost*paSalesTax);
    //prints total cost of belts
    
    
    
  }
}