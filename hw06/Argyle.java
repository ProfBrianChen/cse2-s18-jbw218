import java.util.Scanner;
public class Argyle {
    //Argyle using loops
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int vheight=30;
    int vwidth=80;
    int awidth=8;
    int stripewidth=3;
    String p1=".";
    String p2="+";
    String p3="#";
    System.out.println("Please enter a positive integer for the width of Viewing window in characters. ");
    while (!scan.hasNextInt()){
    System.out.println("Invalid value for width viewing window: ");
    vwidth=scan.nextInt();
    }
    vwidth=scan.nextInt();
    System.out.println("Please enter a positive integer for the height of Viewing window in characters. ");
    while (!scan.hasNextInt()){
    System.out.println("Invalid value for height of viewing window: ");
    vheight=scan.nextInt();
    }
    vheight=scan.nextInt();
    System.out.println("Please enter a positive integer for the width of the argyle diamonds. ");
    while (!scan.hasNextInt()){
    System.out.println("Invalid value for width of argyle diamonds: ");
    awidth=scan.nextInt();
    }
    awidth=scan.nextInt();
    System.out.println("Please enter a positive odd integer for the width of the argyle center stripe. ");
    while (!scan.hasNextInt()){
    System.out.println("Invalid value for width of argyle: ");
    stripewidth=scan.nextInt();
    }
    stripewidth=scan.nextInt();
    System.out.println("Please enter a first character for the pattern fill. ");
    while (p1.length()!=1){
    System.out.println("Invalid character: ");
    }
    p1=scan.next();
    System.out.println("Please enter a second character for the pattern fill. ");
    while (p2.length()!=1){
    System.out.println("Invalid character: ");
    }
    p2=scan.next();
    System.out.println("Please enter a third character for the stripe fill. ");
    while (p3.length()!=1){
    System.out.println("Invalid character: ");
    }
    p3=scan.next();
    
    for (int i=0; i<vheight; i++){
      for (int j=0; j<vwidth; j++){ 
        if ((j%(2*awidth)<stripewidth-1+i || (j+stripewidth-1+i)%(2*awidth)<stripewidth-1+i) &&
           (i<=j%(2*awidth)+1 && j%(2*awidth)<2*awidth-i+1)){
            
          System.out.print(p3);
        }
        else if (j%(2*awidth)>awidth-i-1 && j%(2*awidth)<awidth+i){ 
          System.out.print(p2);
        }
        else{
          System.out.print(p1);
        }
      }
      System.out.println();
    }
    
  }
}
